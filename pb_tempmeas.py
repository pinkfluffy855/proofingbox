#!/usr/bin/python3
import threading
import time
import datetime
import os
import errorlog
import socket
import adafruit_dht
import board


class MyThread(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.t0= datetime.datetime(2021,1,1,0,0,0)
        self.stopped = event
        self.pin = board.D17  
        self.sensor = adafruit_dht.DHT11(self.pin)
        self.bdir0 =r'/home/pi/data'
        self.statfn =r'/home/pi/ctrl/temp.dat'
        self.setpoint =  0
        self.hstat = 0

    def log(self,ts,s):
        try:
            yyyymm = ts.strftime('%Y%m')
            
            self.bdir = os.path.join(self.bdir0,yyyymm)
            if not os.path.isdir(self.bdir):
                os.mkdir(self.bdir)
            yyyymmdd = ts.strftime('%Y%m%d')
            self.fn = os.path.join(self.bdir,'pb_'+yyyymmdd+'.dat')
            if os.path.isfile(self.fn):
                fi = open(self.fn,'a')
            else:
                fi = open(self.fn,'w')
            s1 = ts.strftime('%Y/%m/%d %H:%M:%S')
            s2 = "{0:16.0f}".format((ts-self.t0).total_seconds())
            fi.write(s1+'\t'+s2+'\t'+s)
            fi.close()        
            #print(s1+'\t'+s2+'\t'+s)
        except:
            errorlog.log('Problem in tempmeas log')
            
    def stat(self,ts,s):
        fi = open(self.statfn,'w')
        s1 = ts.strftime('%Y/%m/%d %H:%M:%S')
        s2 = "{0:16.0f}".format((ts-self.t0).total_seconds())
        fi.write(s1+'\t'+s2+'\t'+s)
        fi.close()

    def run(self):
        while not self.stopped.wait(2):
            try: 
                humidity = self.sensor.humidity
                temperature = self.sensor.temperature
                if humidity is not None  and temperature is not None:
                    mytime = datetime.datetime.now()
                    s="{0:6.0f}\t{1:6.0f}\t{2:6.1f}\t{3:4d}\n".\
                    format(temperature,humidity,self.setpoint,self.hstat)
                    self.log(mytime,s)
                    self.stat(mytime,s)
            except:
                errorlog.log('Error reading DHT11')
        errorlog.log('Stopping tempmeas')
        
class ComThread(threading.Thread):
    def __init__(self, event, measThread):
        threading.Thread.__init__(self)
        self.stopped = event
        self.measThread = measThread
        
    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ('localhost', 10000)
        errorlog.log('starting up on {} port {}'.format(*server_address))
        sock.bind(server_address)    
        sock.listen(1)
        line =''
        while not self.stopped.is_set(): 
             connection, client_address = sock.accept()
             print(client_address)
             errorlog.log('connection!')
             try:
                while True:
                    data = connection.recv(1)
                    if data:
                        data = data.decode("utf-8") 
                        if data=='\n':
                            fields = line.split()
                            if len(fields)==2:
                                self.measThread.setpoint = float(fields[0])
                                self.measThread.hstat = int(fields[1])
                            else:
                                print("field length not 2")
                                print(fields)
                            line=''
                        else:
                            line =line+data           
                    else:
                        break
        
             finally:
                connection.close()
            
        
    

def main():
    stopFlag = threading.Event()
    thread = MyThread(stopFlag)
    comthread = ComThread(stopFlag,thread)
    thread.start()
    comthread.start()
    errorlog.log('Starting tempmeas')
    

if __name__ == "__main__":
    main()

