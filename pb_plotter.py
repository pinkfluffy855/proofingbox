#!/usr/bin/python3
# Program to plot the tempperature
import matplotlib
import os
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import errorlog


bdir0 =r'/home/pi/data'
odir =r'/home/pi/plots'
#bdir0 =  r'S:\data'
#odir  =  r'S:\myProjects\tmp'  # output directory for graph


def readfile(ts,bdir0,mintime,time =[],temp=[],humid=[],t2=[],sp=[],he=[]):
    t0= datetime.datetime(2021,1,1,0,0,0) # the time the seconds start
    mins = (mintime - t0).total_seconds()
    yyyymm = ts.strftime('%Y%m')
    bdir = os.path.join(bdir0,yyyymm)
    yyyymmdd = ts.strftime('%Y%m%d')
    fn = os.path.join(bdir,'pb_'+yyyymmdd+'.dat')
    try:
        fi = open(fn)
        for lines in fi:
            fields = lines.split()
            tt = float(fields[2])
            if tt>mins:
                time.append(tt)
                temp.append(float(fields[3]))
                humid.append(float(fields[4]))
            if tt>mins and len(fields)>=6:
                t2.append(tt)
                sp.append(float(fields[5]))
                he.append(float(fields[6]))
    except:
        errorlog.log('Cannot find {0})'.format(fn))
    return time,temp,humid,t2,sp,he
            
def average(ti,te,hu,N=5):
    tin =[]
    ten =[]
    hun =[]
    asum=0
    bsum=0
    csum=0
    n=0
    for a,b,c in zip(ti,te,hu):
        asum+=a 
        bsum+=b 
        csum+=c 
        n+=1
        if n==N:
            tin.append(asum/N)
            ten.append(bsum/N)
            hun.append(csum/N)
            asum=0
            bsum=0
            csum=0
            n=0
    return tin,ten,hun
        
def subtractt0(t,baset):
    t0= datetime.datetime(2021,1,1,0,0,0) # the time the seconds start
    baset0 = (baset - t0).total_seconds()
    nt =[]
    for a in t:
        nt.append((a-baset0)/3600)
    return nt
                          

def duty(t2,he):
    mul=10
    tave =[]
    dave =[]
    for i in range(-24*mul,0):
        h=i/mul
        stored = False
        spave=0
        spco=0
        tsum =0
        for a,b in zip(t2,he):
            if a>h and a<h+1:
                spave +=b
                spco +=1
                tsum +=a
            if a>=h+1 and not stored:
                stored=True
                if spco>0:
                    dave.append(spave/spco*100)
                    tave.append(tsum/spco)
    return tave,dave


def  main():
    errorlog.log('Starting plotter')
    now = datetime.datetime.now()
    yesterday = now-datetime.timedelta(days=1)
    t =[]
    te=[]
    hu =[]
    t2=[]
    sp=[]
    he=[]
    t,te,hu,t2,sp,he = readfile(yesterday,bdir0,yesterday,t,te,hu,t2,sp,he)
    t,te,hu,t2,sp,he = readfile(now,bdir0,yesterday,t,te,hu,t2,sp,he)
    t,te,hu = average(t,te,hu,10)
    t2,sp,he = average(t2,sp,he,10)
    t = subtractt0(t,now)
    t2 = subtractt0(t2,now)
    tave,dc = duty(t2,he)
    fig = plt.figure(1)
    ax = fig.add_axes((0.15,0.15,0.75,0.55))
    ax2 = fig.add_axes((0.15,0.7,0.75,0.1))
    ax2.set_xlim(-25,0)
    ax2.set_ylim(0,100)
    ax.set_xlim(-25,0)
    ax2.grid()
    ax2.xaxis.set_ticklabels([])
    ax2.tick_params(axis='x',direction='inout')
    ax2.set_yticks([50,100])
    ax.grid()
    axy= ax2.twiny()
    axx = ax.twinx()
    ax.plot(t,te,'r.')
    ax.plot(t2,sp,'b-')
    ax2.plot(tave,dc,'g-')
    ax.set_ylim((18,40))
    ax.set_yticks([18,20,22,24,26,28,30,32,34,36,38])
    loC,hiC=  ax.get_ylim()
    lot,hit=  ax.get_xlim()
    loF = loC*9.0/5+32
    hiF = hiC*9.0/5+32
    axx.set_ylim((loF,hiF))
    axx.set_ylabel('T/F')
    ax.set_xlabel('hours ago')
    ax.set_ylabel('T/C')
    ax2.set_ylabel('duty/%')
    axy.set_xlim((lot,hit))
    axy.set_xticks([0])
    axy.set_xticklabels([now.strftime('%m/%d/%Y\n %H:%M:%S')])
    #ax.text(hit,hiC+0.05*(hiC-loC),now.strftime('%m/%d/%Y %H:%M:%S'),horizontalalignment='right')
    #fig.suptitle('Temperature trace at {0}'.format(now.strftime('%m/%d/%Y %H:%M:%S')))
    fn = os.path.join(odir,'temp.png')
    fig.savefig(fn)
    errorlog.log('done')
        
    
    
    
    
    
if __name__ == "__main__":
    main()    
    
