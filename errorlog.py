# module to log errors
import datetime
import os



def log(s,bdir0 = r'/home/pi/errors'):
    try:
        now = datetime.datetime.now()
        yyyymm = now.strftime('%Y%m')
        bdir = os.path.join(bdir0,yyyymm)
        if not os.path.isdir(bdir):
            os.mkdir(bdir)
        yyyymmdd = now.strftime('%Y%m%d')
        fn = os.path.join(bdir,'error_'+yyyymmdd+'.log')    
        if os.path.isfile(fn):
            fi = open(fn,'a')
        else:
            fi = open(fn,'w')
        s1 = now.strftime('%Y/%m/%d %H:%M:%S')
        fi.write(s1+'\t'+s+'\n')
        fi.close()        
        print(s1+'\t'+s)    
        return
    except:
        print("This is now really a problem")
        print("The errorloger is not working")
        print("I was supprot to report: "+s )
    
