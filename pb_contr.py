#!/usr/bin/python3
import threading
import time
import datetime
import os
import errorlog
import board
import digitalio
from enum import Enum
import socket

RELAYS_FAN_GPIO = 13
RELAYS_HEAT_GPIO = 15


class State(Enum):
    ON = 1
    OFF =0

class MyThread(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.t0= datetime.datetime(2021,1,1,0,0,0)
        self.lastchange= self.t0
        self.stopped = event
        self.bdir0 =r'/home/pi/data'
        self.statfn =r'/home/pi/ctrl/temp.dat'
        self.out_of_date = 30  # reading must eb 15 seconds old
        self.alter_state = 10 # wait at least that many seconds before alter change
        self.setpoint = 35
        self.laststate = State.OFF
        self.temp=0
        self.sp='/home/pi/ctrl/setp.dat'
        self.tdict={}
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ('localhost', 10000)
        print('connecting to {} port {}'.format(*server_address))
        self.sock.connect(server_address)
        self.heat = digitalio.DigitalInOut(board.D27)
        self.fan  = digitalio.DigitalInOut(board.D22)
        self.fan.direction  = digitalio.Direction.OUTPUT
        self.heat.direction = digitalio.Direction.OUTPUT
        self.off()

       
 
        
    def readctrldict(self):
        try:
            self.tdict={}
            fi = open(self.sp,'r')
            for lines in fi:
                if lines[0]=='#':
                    continue
                if len(lines)<16:
                    continue
                f = lines.split()
                if len(f)<3:
                    continue
                date = datetime.datetime.strptime(f[0]+' '+f[1],'%m/%d/%Y %H:%M')
                self.tdict[date] = float(f[2])
            fi.close()
        except:
            errorlog.log("problem in controller, readctrldict")
                
    def getsetpoint(self):
        now = datetime.datetime.now()
        ov = 0
        if len(self.tdict)==0:
            self.setpoint = 0
            return
        for k in sorted(self.tdict.keys()):
            if k>now:
                break
            ov = self.tdict[k]
            ok = k
        self.setpoint = ov
        
        return
    
        


    def readstat(self):
        try:
            now = datetime.datetime.now()
            nows = (now-self.t0).total_seconds()
            fi = open(self.statfn,'r')
            l = fi.readline()
            fi.close()
            f = l.split()
            if len(f)<4:
                return False,0,0
            ts = float(f[2])
            te = float(f[3])
            if nows-ts>self.out_of_date:
                errorlog.log('temperature out of date ts={0} nows={1}'.\
                             format(ts,nows))
                return False,ts-nows, te
            return True,ts-nows, te
        except:
            errorlog.log('problem in controller in readstat')            
            return False,ts-nows, te
            
        

    def log(self,ts,s):
        try:
            yyyymm = ts.strftime('%Y%m')
            
            self.bdir = os.path.join(self.bdir0,yyyymm)
            if not os.path.isdir(self.bdir):
                os.mkdir(self.bdir)
            yyyymmdd = ts.strftime('%Y%m%d')
            self.fn = os.path.join(self.bdir,'pb_'+yyyymmdd+'.dat')
            if os.path.isfile(self.fn):
                fi = open(self.fn,'a')
            else:
                fi = open(self.fn,'w')
            s1 = ts.strftime('%Y/%m/%d %H:%M:%S')
            s2 = "{0:16.0f}".format((ts-self.t0).total_seconds())
            fi.write(s1+'\t'+s2+'\t'+s)
            fi.close()        
            #print(s1+'\t'+s2+'\t'+s)
        except:
            errorlog.log('Problem in tempmeas log')
            
    def stat(self,ts,s):
        fi = open(self.statfn,'w')
        s1 = ts.strftime('%Y/%m/%d %H:%M:%S')
        s2 = "{0:16.0f}".format((ts-self.t0).total_seconds())
        fi.write(s1+'\t'+s2+'\t'+s)
        fi.close()

    def off(self):
        self.heat.value = True
        self.fan.value = True 
        self.lastchange = datetime.datetime.now()
        if self.laststate == State.ON:
            errorlog.log('Heat Off @ {0:3.0f}'.format(self.temp))
        self.laststate = State.OFF
        

    def on(self):
        self.heat.value = False
        self.fan.value = False 
        self.lastchange = datetime.datetime.now()
        if self.laststate == State.OFF:
            errorlog.log('Heat On @ {0:3.0f}'.format(self.temp))
        self.laststate = State.ON
                

    def run(self):
        co=0
        while not self.stopped.wait(3):
            if co%10==0:
                self.readctrldict()
            self.getsetpoint()
            co=co+1
            valid,ts,te = self.readstat()
            if valid:
                now = datetime.datetime.now()
                elapsed_seconds = (now-self.lastchange).total_seconds()
                if elapsed_seconds>self.alter_state:
                    self.temp =te
                    if te<self.setpoint:
                        self.on()
                    else:
                        self.off()
                if self.laststate == State.ON:       
                    mstr = '{0:4.1f} 1\n'.format(self.setpoint)
                else:
                    mstr = '{0:4.1f} 0\n'.format(self.setpoint)
                #print("setpoint :",self.setpoint)
                #print(mstr)
                message = bytes(mstr,'utf-8')            
                self.sock.sendall(message)
        errorlog.log('Stopping controller')
        
            


def main():
    stopFlag = threading.Event()
    thread = MyThread(stopFlag)
    thread.start()
    errorlog.log('Starting controller')
    

if __name__ == "__main__":
    main()

