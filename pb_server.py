#!/usr/bin/python3
## Test
#import bottle
from bottle import Bottle, template,route,run,static_file,redirect
import random as rand
import time
import os
import errorlog
import datetime
import sys
import socket

class Server:
    def __init__(self, host, port,hsdir='/home/pi/proofingbox',\
                 sdir='/home/pi/plots',edir='/home/pi/errors',\
                 ctrlfile = '/home/pi/ctrl/setp.dat',
                 ptemp=22,
                 onpi=True):
        """
        webserver for the prrofing box
        static files are served out of sdir and
        home static files out of hsdir. This is where the css and the index 
        are located.
        edir is where the errors are logged
        The ctrlfile is the location of setp.dat\
        ptemp is the standard proofing temperature
        """
        self._host = host
        self._port = port
        self._app = Bottle()
        self._route()
        self.hsdir = hsdir
        self.sdir = sdir
        self.edir = edir
        self.ctrlfile = ctrlfile
        self.ptemp = ptemp
        self.onpi = onpi

    def _route(self):
        self._app.route('/', method="GET", callback=self._index)
        self._app.route('/hello/<name>', callback=self._hello)
        self._app.route('/placeholder', callback=self._placeholder)
        self._app.route('/static/<filename>', callback=self._static)
        self._app.route('/hstatic/<filename>', callback=self._hstatic)
        self._app.route('/startproofing/<n>', callback=self._hourproof)
        self._app.route('/stopproofing', callback=self._stopproof)
        self._app.route('/tempincrease', callback=self._tempincrease)
        self._app.route('/endserver', callback=self._endserver)

    def start(self):
        if self.onpi:
            self._app.run(host=self._host, port=self._port,sever='meinheld')#,debug=True)
        else:
            self._app.run(host=self._host, port=self._port,sever='meinheld')

    def _index(self):
        msg = "serving {0} from {1}".format('index.html',self.hsdir)
        errorlog.log(msg,self.edir)
        return static_file('index.html',root=self.hsdir)

    def _hello(self, name="Guest"):
        return template('Hello {{name}}, how are you?', name=name)

    def _placeholder(self):
        return "placeholder"

    def _static(self,filename):
        msg = "serving {0} from {1}".format(filename,self.sdir)
        errorlog.log(msg,self.edir)
        return static_file(filename,root=self.sdir)

    def _hstatic(self,filename):
        msg = "serving {0} from {1}".format(filename,self.hsdir)
        errorlog.log(msg,self.edir)
        return static_file(filename,root=self.hsdir)
    
    def _hourproof(self,n):
        msg = "hourproof with n= {0}".format(n)
        errorlog.log(msg,self.edir)
        now = datetime.datetime.now()
        tomorrow = now + datetime.timedelta(hours=int(n))
        nowstr = now.strftime("%m/%d/%Y %H:%M")
        tomorrowstr = tomorrow.strftime("%m/%d/%Y %H:%M")
        my_file = open(self.ctrlfile, "w")
        my_file.write("{0} {1} \n".format(nowstr, self.ptemp))
        my_file.write("{0} {1} \n".format(tomorrowstr, "0"))
        my_file.close()
        return redirect('/')
 
    def _stopproof(self):
        msg = "stopproof"
        errorlog.log(msg,self.edir)
        now = datetime.datetime.now()
        nowstr = now.strftime("%m/%d/%Y %H:%M")
        my_file = open(self.ctrlfile, "w")
        my_file.write("{0} {1} \n".format(nowstr, "0"))
        my_file.close()
        return redirect('/')

    def _tempincrease(self):
        msg = "tempincrease"
        errorlog.log(msg,self.edir)  
        my_file = open(self.ctrlfile, "r")
        for lines in my_file:
            list
            
    def _endserver(self):
        raise (KeyboardInterrupt)
        sys.exit()
        

#run(host='192.168.1.201', port=8080, debug=True)
#run(host='0.0.0.0', port=8080, debug=True)
            
name = socket.gethostname()
if name =='pb2':
    server = Server(host='192.168.1.201', port=8080)
    server.start()
elif name=='Hexadecapole':
# for test on Stephan's windows machine
    server = Server(host='localhost', port=8090,\
                hsdir='s:\MyProjects\proofingbox',\
                sdir='s:\mytemp',\
                edir='s:\mytemp',\
                ctrlfile='s:\mytemp\setp.dat',
                onpi=False)
    server.start()
else:
    print("Don't know that pc")    

#try:        

#except:
 #   print("Exception, Server ended")
